class ComputerPlayer
  attr_reader :name
  attr_reader :board
  attr_accessor :mark

  def initialize(name)
    @name = name
    @mark = :mark
  end

  def display(board)
    @board = board
  end

  def get_move
    random_coordinates = {}
    val = 0

    board.positions.each do |pos|
      next unless @board.empty?(pos)
      return pos if @board.winning_move?(pos, @mark)

      random_coordinates[val] = pos if @board.empty?(pos)

      val += 1
    end

    rng = Random.new
    random_coordinates[rng.rand(val)]
  end
end
