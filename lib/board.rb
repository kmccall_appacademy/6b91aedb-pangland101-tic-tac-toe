class Board
  attr_reader :grid

  def initialize(grid = nil)
    @grid = grid || nil_grid
  end

  def [](pos)
    x, y = pos
    @grid[x][y]
  end

  def []=(pos, entry)
    x, y = pos
    @grid[x][y] = entry
  end

  def empty?(pos)
    self[pos].nil?
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def legal_move?(move)
    return false if move.length != 2

    unless self.empty?([move[0].to_i, move[1].to_i])
      puts("Hey! No cheating!")
      return false
    end

    move.all? { |coordinate| [0, 1, 2].include?(coordinate.to_i) }
  end

  def b_display
    @grid.each do |row|
      row.each do |el|
        el.nil? ? print("- ") : print(el.to_s + " ")
      end
      puts
    end
  end

  def winner
    # # These 3 lines should be used for actual game; it only fails specs
    # count = @grid.map { |row| row.count(&:nil?) }.reduce(:+)
    # return nil if count == 9
    # count.even? ? :X : :O

    return @grid[1][1] if diagonal_complete?

    @grid.each do |row|
      return row[0] if row.all? { |el| !el.nil? && el == row[0] }
    end

    i = 0
    while i < @grid.length
      return grid[0][i] if @grid[0][i] && @grid[0][i] == @grid[1][i] &&
      @grid[1][i] == @grid[2][i]
      i += 1
    end

    nil
  end

  def over?
    return false if @grid == nil_grid

    return true if draw?
    return true if row_complete?
    return true if column_complete?
    return true if diagonal_complete?

    false
  end

  def positions # this placement makes more sense than previous
    position_list = []
    3.times do |row|
      3.times { |col| position_list << [row, col] }
    end
    position_list
  end

  def winning_move?(pos, mark)
    return false unless self[pos].nil?
    place_mark(pos, mark)
    return winner if winner == mark
    place_mark(pos, nil)
    false
  end

  private

  def nil_grid
    [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]]
  end

  def row_complete?
    @grid.each do |row|
      return true if row.all? { |el| !el.nil? && el == row[0] }
    end

    false
  end

  def column_complete?
    i = 0
    while i < @grid.length
      return true if @grid[0][i] && @grid[0][i] == @grid[1][i] &&
      @grid[1][i] == @grid[2][i]

      i += 1
    end

    false
  end

  def diagonal_complete?
    if @grid[0][0] && @grid[0][0] == @grid[1][1] &&
      @grid[1][1] == @grid[2][2]
      return true
    end

    if @grid[2][0] && @grid[2][0] == @grid[1][1] &&
      @grid[0][2] == @grid[1][1]
      return true
    end

    false
  end

  def draw?
    @grid.each do |row|
      return false if row.any?(&:nil?)
    end

    true
  end
end
