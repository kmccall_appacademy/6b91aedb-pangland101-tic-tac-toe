class HumanPlayer
  attr_reader :name
  attr_accessor :mark

  def initialize(name)
    @name = name
    @mark = :mark
  end

  def display(board)
    @board = board
    board.b_display
  end

  def get_move
    puts("where") # I want "Place a move!" but that fails specs
    user_input = gets.chomp
    move = user_input.delete(" ").split(",")

    until @board.legal_move?(move)
      puts("Try again! Format example: \'0, 2\'")
      user_input = gets.chomp
      move = user_input.delete(" ").split(",")
    end

    [move[0].to_i, move[1].to_i]
  end
end
